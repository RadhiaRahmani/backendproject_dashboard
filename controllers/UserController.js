let dbConn = require("../db");
const fs = require("fs");
// get all users (user,employe and admin
exports.getallusers = (req, res) => {
  dbConn.query("SELECT * FROM user_table", (err, rows, fields) => {
    if (!err) res.send(rows.rows);
    else console.log(err);
  });
};

// add user
exports.adduser = (req, res) => {
  const user = req.body;
  console.log(user);
  dbConn.query(
    "insert into user_table (user_id,user_firstname,user_lastname,user_phone,user_email,user_civility,user_speciality,user_adress,user_birthday,user_seniority,user_experience,user_comment,id_role,cin,poste,verified,deleted,user_password,image_profile,creation_date) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18,$19,$20)",
    [
      user.id,
      user.firstname,
      user.lastname,
      user.phone,
      user.email,
      user.civility,
      user.speciality,
      user.adress,
      user.birthday,
      user.seniority,
      user.experience,
      user.comment,
      user.role,
      user.cin,
      user.poste,
      false,
      false,
      "",
      "https://www.pngitem.com/pimgs/m/512-5125598_computer-icons-scalable-vector-graphics-user-profile-avatar.png",
      user.datecreation,
    ],
    (err, rows, fields) => {
      if (!err) res.send("user successfully added");
      else console.log(err);
    }
  );
};
// get all users
exports.getusers = (req, res) => {
  dbConn.query(
    "SELECT * FROM user_table  where id_role=3 and deleted=false",
    (err, rows, fields) => {
      if (!err) res.send(rows.rows);
      else console.log(err);
    }
  );
};
// update user
exports.updateuser = (req, res) => {
  const user = req.body;
  const user_id = req.params.id;
  console.log(user);
  console.log(user_id);
  dbConn.query(
    "update  user_table  SET user_firstname=$1 ,user_lastname =$2 ,user_phone=$3,user_email=$4, user_civility=$5, user_speciality=$6, user_adress=$7,  user_birthday=$8, user_seniority=$9  ,user_experience=$10 , user_comment =$11 , cin=$12 , poste=$13  where user_id =$14 ",
    [
      user.firstname,
      user.lastname,
      user.phone,
      user.email,
      user.civility,
      user.speciality,
      user.adress,
      user.birthday,
      user.seniority,
      user.experience,
      user.comment,
      user.cin,
      user.poste,
      user_id,
    ],
    (err, rows, fields) => {
      if (!err) res.send("user successfully updated");
      else console.log(err);
    }
  );
};

// delete user
exports.deleteuser = (req, res) => {
  const user_id = req.params.id;
  dbConn.query(
    "update  user_table  set deleted=true where user_id =$1 ",
    [user_id],
    (err, rows, fields) => {
      if (!err) res.send("user successfully deleted");
      else console.log(err);
    }
  );
};
// get user by id
exports.getuserbyid = (req, res) => {
  const user_id = req.params.id;
  console.log(user_id);
  dbConn.query(
    "select * from  user_table   where user_id =$1 ",
    [user_id],
    (err, rows, fields) => {
      if (!err) res.send(rows.rows);
      else console.log(err);
    }
  );
};

exports.updateuserpassword = (req, res) => {
  dbConn.query(
    "update  user_table  set user_password =$1  where user_id =$2 ",
    [req.body.code, req.body.id],
    (err, rows, fields) => {
      if (!err) res.send(rows.rows);
      else console.log(err);
    }
  );
};

// edit user password
exports.edituserpassword = async (req, res) => {
  const user = req.params.id;
  const pass = req.body.password;
  const bcrypt = require("bcrypt");
  const hashedPassword = await bcrypt.hash(pass, 10);
  console.log("password" + hashedPassword);
  console.log("user" + user);

  dbConn.query(
    "update  user_table	 set user_password =$1 where user_id =$2  ",
    [hashedPassword, user],
    (err, rows, fields) => {
      if (!err) {
        res.send(rows);
        console.log(rows.rows);
      } else console.log(err);
    }
  );
};

// validate user informations
exports.validateuser = (req, res) => {
  const user = req.body;
  const user_id = req.params.id;
  console.log("userrr" + req.body);
  console.log("user" + user_id);
  dbConn.query(
    "update  user_table  set user_firstname=$1 ,user_lastname =$2 ,user_phone=$3,user_email=$4, user_civility=$5, user_speciality=$6, user_adress=$7,  user_birthday=$8, user_seniority=$9 ,user_experience=$10 , user_comment =$11 , cin=$12 , poste=$13 ,verified=$14 where user_id =$15 ",
    [
      user.firstname,
      user.lastname,
      user.phone,
      user.email,
      user.civility,
      user.speciality,
      user.adress,
      user.birthday,
      user.seniority,
      user.experience,
      user.comment,
      user.cin,
      user.poste,
      true,
      user_id,
    ],
    (err, rows, fields) => {
      if (!err) res.send("user successfully updated");
      else console.log(err);
    }
  );
};

// get user images
exports.getuserpictures = (req, res) => {
  dbConn.query(
    "select * from user_images where user_id=$1  ",
    [req.params.id],
    (err, rows, fields) => {
      if (!err) res.send(rows.rows);
      else console.log(err);
    }
  );
};
// add new image
exports.addimage = async (req, res) => {
  const Jimp = require("jimp");
  const id = req.params.id;
  // add new path to store images
  const newpath = "./public/images/";
  const file = req.files.file;
  const filename = file.name;
  console.log(filename);
  file.mv(`${newpath}${filename}`);
  // add image to database
  dbConn.query(
    "insert into user_images(user_id,img_name)  values($1,$2)",
    [id, filename],
    (err, rows, fields) => {
      if (!err) res.send("added");
      else console.log(err);
    }
  );
};
// add image logo
exports.addlogo = async (req, res) => {
  const Jimp = require("jimp");
  const img = req.body.filename;
  console.log(img);
  // Reading image
  const image = await Jimp.read(`./public/images/${img}`);
  // Defining the text font
  const font = await Jimp.loadFont(Jimp.FONT_SANS_32_BLACK);
  image.print(font, 5, 10, "PRIVATE");
  // Writing image after processing
  await image.writeAsync(`./public/images_withlogo/${img}`);
};

// add new image
// exports.addimage = (req, res) => {
//   const multer = require("multer");
//   const id = req.params.id;
//   const DIR = "./public/";
//   var storage = multer.diskStorage({
//     destination: (req, file, cb) => {
//       cb(null, DIR);
//     },
//     filename: (req, file, cb) => {
//       const fileName = file.originalname.toLowerCase().split(" ").join("-");
//       cb(null, uuidv4() + "-" + fileName);
//     },
//   });

//   var upload = multer({
//     storage: storage,
//     fileFilter: (req, file, cb) => {
//       if (
//         file.mimetype == "image/png" ||
//         file.mimetype == "image/jpg" ||
//         file.mimetype == "image/jpeg"
//       ) {
//         cb(null, true);
//       } else {
//         cb(null, false);
//         return cb(new Error("Only .png, .jpg and .jpeg format allowed!"));
//       }
//     },
//   });

//   app.post("/api/image", upload.single("image"), (req, res, err) => {
//     if (
//       !req.file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)
//     ) {
//       res.send({ msg: "Only image files (jpg, jpeg, png) are allowed!" });
//     }

//     const image = req.file.filename;
//     const sqlInsert =
//       "insert into user_images(user_id,img_name)  values($1,$2)";

//     db.query(sqlInsert, [id,image], (err, rows) => {
//       if (err) {
//         console.log(err);
//       }
//       if (rows) {
//         res.send({ data: rows.rows });
//       }
//     });
//   });
// };
// get content by  language
exports.getcontentbylanguage = (req, res) => {
  const id = req.params.id;
  dbConn.query(
    "SELECT * FROM  language_content where id_lang =$1",
    [id],
    (err, rows, fields) => {
      if (!err) res.send(rows.rows);
      else console.log(err);
    }
  );
};
// get selected language
exports.getlanguage = (req, res) => {
  const id = req.params.id;
  dbConn.query(
    "SELECT * FROM  language where id_lang =$1",
    [id],
    (err, rows, fields) => {
      if (!err) res.send(rows.rows[0]);
      else console.log(err);
    }
  );
};
// get all employers
exports.getallemployes = (req, res) => {
  dbConn.query(
    "SELECT * FROM user_table where id_role=2 and deleted=false",
    (err, rows, fields) => {
      if (!err) res.send(rows.rows);
      else console.log(err);
    }
  );
};
// login
exports.login = (req, res) => {
  const phone = req.body.phone;
  const bcrypt = require("bcrypt");
  console.log(phone);
  const password = req.body.password;
  dbConn.query(
    "SELECT * FROM user_table where  user_phone=$1",
    [phone],
    (err, rows, fields) => {
      if (rows.rows.length === 0) {
        console.log("--------> User does not exist");
        res.sendStatus(404);
      } else {
        const hashedPassword = rows.rows[0].user_password;
        if (bcrypt.compare(password, hashedPassword)) {
          console.log("---------> Login Successful");
          res.send(rows.rows);
        } else {
          console.log("---------> Password Incorrect");
          res.send("Password incorrect!");
        } //end of bcrypt.compare()
      }
    }
  );
};
exports.createadmin = async (req, res) => {
  const bcrypt = require("bcrypt");
  const firstname = req.body.firstname;
  const lastname = req.body.lastname;
  const email = req.body.email;
  const phone = req.body.phone;
  const password = req.body.password;
  const hashedPassword = await bcrypt.hash(password, 10);
  console.log(firstname);
  console.log(lastname);
  console.log(hashedPassword);
  console.log(phone);
  console.log(email);
  dbConn.query(
    "insert into user_table (user_firstname,user_lastname,user_phone,user_email,user_civility,user_speciality,user_adress,user_birthday,user_seniority,user_experience,user_comment,id_role,cin,poste,verified,deleted,	user_password,image_profile) values ($1,$2,$3,$4,$5,$6,$7,$8,$9,$10,$11,$12,$13,$14,$15,$16,$17,$18)",
    [
      firstname,
      lastname,
      phone,
      email,
      "",
      "",
      "",
      "",
      "",
      "",
      "",
      1,
      "",
      "",
      false,
      false,
      hashedPassword,
      "https://www.pngitem.com/pimgs/m/512-5125598_computer-icons-scalable-vector-graphics-user-profile-avatar.png",
    ],
    (err, rows, fields) => {
      if (!err) res.send(rows.rows);
      else console.log(err);
    }
  );
};
// delete user image
exports.deleteimage = (req, res) => {
  const image_id = req.params.id;
  console.log(image_id);
  dbConn.query(
    "delete from  user_images where id_img=$1",
    [image_id],
    (err, rows, fields) => {
      if (!err) console.log("image deleted");
      else console.log(err);
    }
  );
};
exports.getmaxid = (req, res) => {
  dbConn.query("select MAX(user_id) from user_table"),
    (err, rows, fields) => {
      if (!err) res.send(rows.rows[0]);
      else console.log(err);
    };
};
